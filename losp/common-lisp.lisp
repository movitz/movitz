;;;;------------------------------------------------------------------
;;;; 
;;;;    Copyright (C) 20012000, 2004,
;;;;    Department of Computer Science, University of Tromso, Norway
;;;; 
;;;; Filename:      common-lisp.lisp
;;;; Description:   
;;;; Author:        Frode Vatvedt Fjeld <frodef@acm.org>
;;;; Created at:    Tue Dec  5 18:33:01 2000
;;;; Distribution:  See the accompanying file COPYING.
;;;;                
;;;; $Id: common-lisp.lisp,v 1.1.1.1 2004/01/13 11:05:04 ffjeld Exp $
;;;;                
;;;;------------------------------------------------------------------

(require :muerte/common-lisp)
(provide :common-lisp)
